<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:47
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <%@ page contentType="text/html;charset=UTF-8" language="java" %>
    <title>Edit customer</title>
</head>
<body>
<h1>Edit customer</h1>

<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>

<form method="post">
    <fieldset>
        <legend>Customer information</legend>
        <table>
            <tr>
                <td>Name:</td>
                <td><input type="text" name="customerName" value="${requestScope["customer"].getCustomerName()}"></td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td><input type="text" name="phone" value="${requestScope["customer"].getPhone()}"></td>
            </tr>
            <tr>
                <td>Address:</td>
                <td><input type="text" name="addressLine" value="${requestScope["customer"].getAddressLine()}"></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><input type="text" name="city" value="${requestScope["customer"].getCity()}"></td>
            </tr>
            <tr>
                <td>State:</td>
                <td><input type="text" name="state" value="${requestScope["customer"].getState()}"></td>
            </tr>
            <tr>
                <td>Postal Code:</td>
                <td><input type="text" name="postalCode" value="${requestScope["customer"].getPostalCode()}"></td>
            </tr>
            <tr>
                <td>Country:</td>
                <td><input type="text" name="country" value="${requestScope["customer"].getCountry()}"></td>
            </tr>
            <tr>
                <td>salesRepEmployeeNumber:</td>
                <td>
                    <select name="listEmployeeNumber">
                        <c:forEach items='${requestScope["employees"]}' var="employee">
                            <option value="${employee.getEmployeeNumber()}"
                                ${requestScope["customer"].getSalesRepEmployeeNumber() == employee.getEmployeeNumber() ? "selected" : ""}>
                                    ${employee.getEmployeeNumber()}
                            </option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" value="Update customer"></td>
                <td><a href="/customers">Back to customer list</a></td>
            </tr>
        </table>
    </fieldset>
</form>
</body>
</html>

