<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: muoi
  Date: 05/11/2019
  Time: 15:46
  To change this template use File | Settings | File Templates.
--%>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Create new customer</title>
    <style>
        .message {
            color: green;
        }
    </style>
</head>
<body>
<h1>Create new customer</h1>
<p>
    <c:if test='${requestScope["message"] != null}'>
        <span class="message">${requestScope["message"]}</span>
    </c:if>
</p>
<p>
    <a href="/customers">Back to customer list</a>
</p>
<form method="post">
    <fieldset>
        <legend>Customer information</legend>
        <table>
            <tr>
                <td>Name:</td>
                <td><input type="text" name="customerName"></td>
            </tr>
            <tr>
                <td>Phone:</td>
                <td><input type="text" name="phone" id="phone"></td>
            </tr>
            <tr>
                <td>Address:</td>
                <td><input type="text" name="addressLine" id="email"></td>
            </tr>
            <tr>
                <td>City:</td>
                <td><input type="text" name="city"></td>
            </tr>
            <tr>
                <td>State:</td>
                <td><input type="text" name="state"></td>
            </tr>
            <tr>
                <td>Postal Code:</td>
                <td><input type="text" name="postalCode"></td>
            </tr>
            <tr>
                <td>Country:</td>
                <td><input type="text" name="country"></td>
            </tr>
            <tr>
                <td>salesRepEmployeeNumber:</td>
                <td>
                    <select name="listEmployeeNumber">
                        <c:forEach items='${requestScope["employees"]}' var="employee">
                            <option value="${employee.getEmployeeNumber()}">${employee.getEmployeeNumber()}</option>
                        </c:forEach>
                    </select>
                </td>
            </tr>
            <tr>
                <td></td>
                <td><input type="submit" value="Create customer" onclick="return validate()"></td>
            </tr>
        </table>
    </fieldset>
    <script type="text/javascript">
        function validate() {
            // var regexp = /^[A-Za-z0-9]+@[A-Za-z0-9]+\.[A-Za-z0-9]+$/,
            //     email = document.getElementById("email").value;
            //
            // if (regexp.test(email))
            //     return true;
            // else {
            //     alert("email wrong");
            //     return false;
            // }

            // var regexp2 = /^(09|01[2|6|8|9])+([0-9]{8})+$/,
            //     phone = document.getElementById("phone").value;
            // if (regexp2.test(phone)) {
            //     return true;
            // } else {
            //     alert("phone number wrong");
            //     return false;
            // }
        }
    </script>
</form>
</body>
</html>
