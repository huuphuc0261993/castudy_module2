package service.customers;

import model.Customer;
import service.customers.CustomerService;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class CustomerJDBCServiceImpl implements CustomerService {
    private String jdbcURL = "jdbc:mysql://localhost:3306/classicmodels";
    private String jdbcUsername = "huuphuc0261993";
    private String jdbcPassword = "Cccv0103@";

    private static final String INSERT_USER_SQL = "INSERT INTO Customers" + "(Fullname, Identity_card, Dob, Email, Phone, Country, Booking_Deposit_ID, " +
            "created_at, created_by) VALUES"
            + "(?, ?, ?, ?, ?, ?, ?, ?, ?);";
    private static final String SELECT_USER_BY_ID = "SELECT ID,Fullname, Identity_card, Dob, Email, Phone, Country,Booking_Deposit_ID FROM Customers WHERE ID = ?;";
    private static final String SELECT_ALL_USERS = "SELECT * FROM Customers WHERE isDelete = 0;";
    private static final String DELETE_USER_SQL = "UPDATE Customers SET isDelete = 1, deleted_at = ?, deleted_by = ? WHERE ID = ?;";
    private static final String UPDATE_USER_SQL = "UPDATE Customers SET Fullname = ?, Identity_card = ?, Dob = ?, Email = ?, Phone = ?, Country = ?, country = ?, Booking_Deposit_ID = ?, updated_at = ?, updated_by = ? WHERE ID = ?;";

    protected Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return connection;
    }

    private void printSQLException(SQLException ex) {
        for (Throwable e : ex) {
            if (e instanceof SQLException) {
                e.printStackTrace(System.err);
                System.err.println("SQLState: " + ((SQLException) e).getSQLState());
                System.err.println("Error Code: " + ((SQLException) e).getErrorCode());
                System.err.println("Message: " + e.getMessage());
                Throwable t = ex.getCause();
                while (t != null) {
                    System.out.println("Cause: " + t);
                    t = t.getCause();
                }
            }
        }
    }

    @Override
    public List<Customer> findAll() {
        List<Customer> users = new ArrayList<>();
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_USERS)) {
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();
            while (rs.next()) {
                int ID = rs.getInt("ID");
                String Fullname = rs.getString("Fullname");
                String Identity_card = rs.getString("Identity_card");
                String Dob = rs.getString("Dob");
                String Email = rs.getString("Email");
                String Phone = rs.getString("Phone");
                String Country = rs.getString("Country");
                int Booking_Deposit_ID = rs.getInt("Booking_Deposit_ID");
                users.add(new Customer(ID, Booking_Deposit_ID, Fullname, Identity_card, Dob, Email, Phone, Country));
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return users;
    }

    @Override
    public void save(Customer customer) {
        System.out.println(INSERT_USER_SQL);
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER_SQL)) {
            preparedStatement.setString(1, customer.getFullname());
            preparedStatement.setString(2, customer.getPhone());
            preparedStatement.setString(3, customer.getIdentity_card());
            preparedStatement.setString(4, customer.getDob());
            preparedStatement.setString(5, customer.getEmail());
            preparedStatement.setString(6, customer.getPhone());
            preparedStatement.setString(7, customer.getCountry());
            preparedStatement.setInt(8, customer.getBooking_Deposit_ID());
            customer.setCreated_at();
            preparedStatement.setString(9, customer.getCreated_at());
            preparedStatement.setString(10, customer.getCreated_by());

            System.out.println(preparedStatement);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public Customer findById(int ID) {
        Customer customer = null;
        try (Connection connection = getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_ID)) {
            preparedStatement.setInt(1, ID);
            System.out.println(preparedStatement);
            ResultSet rs = preparedStatement.executeQuery();

            while (rs.next()) {
                String Fullname = rs.getString("Fullname");
                String Identity_card = rs.getString("Identity_card");
                String Dob = rs.getString("Dob");
                String Email = rs.getString("Email");
                String Phone = rs.getString("Phone");
                String Country = rs.getString("Country");
                int Booking_Deposit_ID = rs.getInt("Booking_Deposit_ID");
                customer = new Customer(ID, Booking_Deposit_ID, Fullname, Identity_card, Dob, Email, Phone, Country);
            }
        } catch (SQLException e) {
            printSQLException(e);
        }
        return customer;

    }

    @Override
    public void update(Customer customer) {
        boolean rowUpdated;
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(UPDATE_USER_SQL)) {
            statement.setString(1, customer.getFullname());
            statement.setString(2, customer.getPhone());
            statement.setString(3, customer.getIdentity_card());
            statement.setString(4, customer.getDob());
            statement.setString(5, customer.getEmail());
            statement.setString(6, customer.getPhone());
            statement.setString(7, customer.getCountry());
            statement.setInt(8, customer.getBooking_Deposit_ID());
            customer.setUpdated_at();
            statement.setString(9, customer.getUpdated_at());
            statement.setString(10, customer.getUpdated_by());
            statement.setInt(11, customer.getID());


            rowUpdated = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            printSQLException(e);
        }
    }

    @Override
    public void remove(int ID, Customer customer) {
        try (Connection connection = getConnection();
             PreparedStatement statement = connection.prepareStatement(DELETE_USER_SQL)) {
            customer.setDeleted_at();
            statement.setString(1, customer.getDeleted_at());
            statement.setString(2, customer.getDeleted_by());
            statement.setInt(3, ID);
            statement.executeUpdate();

        } catch (SQLException e) {
            printSQLException(e);
        }
    }
}
