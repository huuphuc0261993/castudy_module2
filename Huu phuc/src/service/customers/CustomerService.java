package service.customers;

import model.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> findAll();

    void save(Customer customer);

    Customer findById(int customerNumber);

    void update(Customer customer);

    void remove(int customerNumber, Customer customer);
}