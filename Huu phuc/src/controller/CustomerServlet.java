package controller;

import model.Customer;
import service.customers.CustomerJDBCServiceImpl;
import service.customers.CustomerService;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "CustomerServlet", urlPatterns = "/customers")
public class CustomerServlet extends HttpServlet {
    private CustomerService customerService = new CustomerJDBCServiceImpl();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String action = request.getParameter("action");
    if(action==null){
        action="";
    }
    switch (action){
        case "create":
            showCreateForm(request, response);
            break;
        case "edit":
            showEditForm(request, response);
            break;
        case "delete":
            showDeleteForm(request, response);
            break;
        case "view":
            viewCustomer(request, response);
            break;
        default:
            listCustomers(request, response);
    }
    }

    private void listCustomers(HttpServletRequest request, HttpServletResponse response) {
        RequestDispatcher dispatcher;
        List<Customer> customers = this.customerService.findAll();
        request.setAttribute("customers", customers);
        dispatcher = request.getRequestDispatcher("customers/list.jsp");

        try {
            dispatcher.forward(request, response);
        } catch (ServletException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
