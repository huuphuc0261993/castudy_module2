package model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Customer {
    private int id;
    private int bookingDepositId;
    private String fullName;
    private String identityCard;
    private String dob;
    private String email;
    private String phone;
    private String country;
    private int isDelete;
    private String deleted_at;
    private String deleted_by = "admin";
    private String updated_at;
    private String updated_by = "admin";
    private String created_at;
    private String created_by = "admin";

    public Customer(int id, int booking_Deposit_ID, String fullname, String identity_card,
                    String dob, String email, String phone, String country) {
        this.id = id;
        bookingDepositId = booking_Deposit_ID;
        fullName = fullname;
        identityCard = identity_card;
        dob = dob;
        email = email;
        phone = phone;
        country = country;
    }

    public Customer(int booking_Deposit_ID, String fullname, String identity_card,
                    String dob, String email, String phone, String country) {
        bookingDepositId = booking_Deposit_ID;
        fullName = fullname;
        identityCard = identity_card;
        dob = dob;
        email = email;
        phone = phone;
        country = country;
    }

    public int getID() {
        return id;
    }

    public void setID(int id) {
        this.id = id;
    }

    public int getBooking_Deposit_ID() {
        return bookingDepositId;
    }

    public void setBooking_Deposit_ID(int booking_Deposit_ID) {
        bookingDepositId = booking_Deposit_ID;
    }

    public String getFullname() {
        return fullName;
    }

    public void setFullname(String fullname) {
        fullName = fullname;
    }

    public String getIdentity_card() {
        return identityCard;
    }

    public void setIdentity_card(String identity_card) {
        identityCard = identity_card;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        dob = dob;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        phone = phone;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        country = country;
    }

    
    private String date() {
//        LocalDateTime localDateTime = LocalDateTime.now();
//        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//        return localDateTime.format(dateTimeFormatter);

        LocalDateTime now = LocalDateTime.now();
        return Timestamp.valueOf(now).toString();
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at() {
        this.deleted_at = date();
    }

    public String getDeleted_by() {
        return deleted_by;
    }

    public void setDeleted_by(String deleted_by) {
        this.deleted_by = deleted_by;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at() {
        this.updated_at = date();
    }

    public String getUpdated_by() {
        return updated_by;
    }

    public void setUpdated_by(String updated_by) {
        this.updated_by = updated_by;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at() {
        this.created_at = date();
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
}
