-- MySQL dump 10.13  Distrib 5.7.27, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: case_study_version1
-- ------------------------------------------------------
-- Server version	5.7.27-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Access_Modifiers`
--

DROP TABLE IF EXISTS `Access_Modifiers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Access_Modifiers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Group` varchar(255) DEFAULT NULL,
  `Description` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Access_Modifiers`
--

LOCK TABLES `Access_Modifiers` WRITE;
/*!40000 ALTER TABLE `Access_Modifiers` DISABLE KEYS */;
INSERT INTO `Access_Modifiers` VALUES (1,'1','Nhân viên',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2','Quản lý',0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Access_Modifiers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Access_permisions`
--

DROP TABLE IF EXISTS `Access_permisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Access_permisions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Group` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Permission` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `Users_ID` int(11) NOT NULL,
  `Access_Modifiers_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_Access_permision_Users1_idx` (`Users_ID`),
  KEY `fk_Access_permisions_Access_Modifiers1_idx` (`Access_Modifiers_ID`),
  CONSTRAINT `fk_Access_permision_Users1` FOREIGN KEY (`Users_ID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Access_permisions_Access_Modifiers1` FOREIGN KEY (`Access_Modifiers_ID`) REFERENCES `Access_Modifiers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Access_permisions`
--

LOCK TABLES `Access_permisions` WRITE;
/*!40000 ALTER TABLE `Access_permisions` DISABLE KEYS */;
INSERT INTO `Access_permisions` VALUES (15,'NV','sửa',2,1);
/*!40000 ALTER TABLE `Access_permisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Booking_Deposits`
--

DROP TABLE IF EXISTS `Booking_Deposits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Booking_Deposits` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Day_deposit` datetime DEFAULT NULL,
  `Payments` varchar(255) DEFAULT NULL,
  `Money_deposit` double DEFAULT NULL,
  `Payments_type` varchar(255) DEFAULT NULL,
  `User_deposit` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Booking_Deposits`
--

LOCK TABLES `Booking_Deposits` WRITE;
/*!40000 ALTER TABLE `Booking_Deposits` DISABLE KEYS */;
INSERT INTO `Booking_Deposits` VALUES (1,'2019-11-10 00:00:00','Chuyển khoản',500000,'mastercard','Nguyễn Văn A',0,NULL,NULL,NULL,NULL,NULL,NULL),(2,'2019-11-10 00:00:00','Tiền mặt',300000,'Tiền mặt','Nguyễn Văn A',0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Booking_Deposits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Booking_Details`
--

DROP TABLE IF EXISTS `Booking_Details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Booking_Details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Room_name` varchar(255) DEFAULT NULL,
  `Type_room` varchar(255) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Money_deposit` double DEFAULT NULL,
  `Money_type` varchar(255) DEFAULT NULL,
  `Day_booking` datetime DEFAULT NULL,
  `User_id_in` varchar(255) DEFAULT NULL,
  `Day_out` datetime DEFAULT NULL,
  `User_id_out` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `Booking_Room_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`),
  KEY `fk_Booking_Details_Booking_Rooms1_idx` (`Booking_Room_ID`),
  CONSTRAINT `fk_Booking_Details_Booking_Rooms1` FOREIGN KEY (`Booking_Room_ID`) REFERENCES `Booking_Rooms` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Booking_Details`
--

LOCK TABLES `Booking_Details` WRITE;
/*!40000 ALTER TABLE `Booking_Details` DISABLE KEYS */;
INSERT INTO `Booking_Details` VALUES (1,'401','vip',1000000,500000,'vnd','2019-11-11 00:00:00','Trần Văn A','2019-11-20 00:00:00','Trằn Văn A',0,NULL,NULL,NULL,NULL,NULL,NULL,1);
/*!40000 ALTER TABLE `Booking_Details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Booking_Rooms`
--

DROP TABLE IF EXISTS `Booking_Rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Booking_Rooms` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Room_number` int(11) DEFAULT NULL,
  `Day_number` int(11) DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Price_total` double DEFAULT NULL,
  `Day_reservation` datetime DEFAULT NULL,
  `Day_check_in` datetime DEFAULT NULL,
  `Day_check_out` datetime DEFAULT NULL,
  `User_id_check_in` int(11) DEFAULT NULL,
  `User_id_check_out` int(11) DEFAULT NULL,
  `Type_room` varchar(255) DEFAULT NULL,
  `TypeRoom_number` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `Bookings_ID` int(11) NOT NULL,
  `TypeRooms_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Booking_Rooms_Bookings1_idx1` (`Bookings_ID`),
  KEY `fk_Booking_Rooms_TypeRooms1_idx1` (`TypeRooms_ID`),
  CONSTRAINT `fk_Booking_Rooms_Bookings1` FOREIGN KEY (`Bookings_ID`) REFERENCES `Bookings` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Booking_Rooms_TypeRooms1` FOREIGN KEY (`TypeRooms_ID`) REFERENCES `TypeRooms` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Booking_Rooms`
--

LOCK TABLES `Booking_Rooms` WRITE;
/*!40000 ALTER TABLE `Booking_Rooms` DISABLE KEYS */;
INSERT INTO `Booking_Rooms` VALUES (1,401,1,1000000,1000000,'2019-11-11 00:00:00','2019-11-12 00:00:00','2019-11-20 00:00:00',1,1,'vip',401,0,NULL,NULL,NULL,NULL,NULL,NULL,1,1);
/*!40000 ALTER TABLE `Booking_Rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Bookings`
--

DROP TABLE IF EXISTS `Bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bookings` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Day_reservation` datetime DEFAULT NULL,
  `Day_check_in` datetime DEFAULT NULL,
  `Day_check_out` datetime DEFAULT NULL,
  `Payments` varchar(255) DEFAULT NULL,
  `Amount_all` double DEFAULT NULL,
  `Money_type` varchar(255) DEFAULT NULL,
  `User_check_in` int(11) DEFAULT NULL,
  `User_check_out` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `Users_ID` int(11) NOT NULL,
  `Booking_Deposits_ID` int(11) NOT NULL,
  `Customers_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Bookings_Users1_idx` (`Users_ID`),
  KEY `fk_Bookings_Booking_Deposits1_idx` (`Booking_Deposits_ID`),
  KEY `fk_Bookings_Customers1_idx` (`Customers_ID`),
  CONSTRAINT `fk_Bookings_Booking_Deposits1` FOREIGN KEY (`Booking_Deposits_ID`) REFERENCES `Booking_Deposits` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bookings_Customers1` FOREIGN KEY (`Customers_ID`) REFERENCES `Customers` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Bookings_Users1` FOREIGN KEY (`Users_ID`) REFERENCES `Users` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Bookings`
--

LOCK TABLES `Bookings` WRITE;
/*!40000 ALTER TABLE `Bookings` DISABLE KEYS */;
INSERT INTO `Bookings` VALUES (1,'2019-11-11 00:00:00','2019-11-12 00:00:00','2019-11-20 00:00:00','Tiền Mặt',1000000,'VND',1,1,0,NULL,NULL,NULL,NULL,NULL,NULL,2,1,1);
/*!40000 ALTER TABLE `Bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Customers`
--

DROP TABLE IF EXISTS `Customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Customers` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fullname` varchar(255) DEFAULT NULL,
  `Identity_card` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Dob` datetime DEFAULT NULL,
  `Email` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Phone` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Country` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Booking_Deposits_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Customers_Booking_Deposits1_idx` (`Booking_Deposits_ID`),
  CONSTRAINT `fk_Customers_Booking_Deposits1` FOREIGN KEY (`Booking_Deposits_ID`) REFERENCES `Booking_Deposits` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Customers`
--

LOCK TABLES `Customers` WRITE;
/*!40000 ALTER TABLE `Customers` DISABLE KEYS */;
INSERT INTO `Customers` VALUES (1,'Trần Hữu Phúc','191769752','1993-06-02 00:00:00','huuphuc0261993@gmail.com','0942345193','Huế',0,NULL,NULL,NULL,NULL,NULL,NULL,1),(2,'Nguyễn Ngọc Linh Đan','123456789','1994-01-01 00:00:00','linhdan@gmail.com','0383477129','Huế',0,NULL,NULL,NULL,NULL,NULL,NULL,2);
/*!40000 ALTER TABLE `Customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Reservations`
--

DROP TABLE IF EXISTS `Reservations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Reservations` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Check_in` datetime DEFAULT NULL,
  `Check_out` datetime DEFAULT NULL,
  `Room_Number` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Reservations`
--

LOCK TABLES `Reservations` WRITE;
/*!40000 ALTER TABLE `Reservations` DISABLE KEYS */;
INSERT INTO `Reservations` VALUES (1,'2019-11-11 00:00:00','2019-11-20 00:00:00',101,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Reservations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Room_details`
--

DROP TABLE IF EXISTS `Room_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Room_details` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Room_name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Reservations_ID1` int(11) NOT NULL,
  `TypeRooms_ID` int(11) NOT NULL,
  PRIMARY KEY (`ID`),
  KEY `fk_Room_details_Reservations1_idx` (`Reservations_ID1`),
  KEY `fk_Room_details_TypeRooms1_idx` (`TypeRooms_ID`),
  CONSTRAINT `fk_Room_details_Reservations1` FOREIGN KEY (`Reservations_ID1`) REFERENCES `Reservations` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_Room_details_TypeRooms1` FOREIGN KEY (`TypeRooms_ID`) REFERENCES `TypeRooms` (`ID`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Room_details`
--

LOCK TABLES `Room_details` WRITE;
/*!40000 ALTER TABLE `Room_details` DISABLE KEYS */;
INSERT INTO `Room_details` VALUES (1,'VIP1',0,NULL,NULL,NULL,NULL,NULL,NULL,1,1);
/*!40000 ALTER TABLE `Room_details` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `TypeRooms`
--

DROP TABLE IF EXISTS `TypeRooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TypeRooms` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `TypeRoom` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Tag` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `Price` double DEFAULT NULL,
  `Summary` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Number` int(11) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `TypeRooms`
--

LOCK TABLES `TypeRooms` WRITE;
/*!40000 ALTER TABLE `TypeRooms` DISABLE KEYS */;
INSERT INTO `TypeRooms` VALUES (1,'VIP','A',1000000,'Đây là phòng vip','Đây là mô tả','url',3,0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `TypeRooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Fullname` varchar(255) DEFAULT NULL,
  `Username` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Password` varchar(255) DEFAULT NULL,
  `Avatar` varchar(255) DEFAULT NULL,
  `isDeleted` tinyint(4) DEFAULT '0',
  `deleted_at` datetime DEFAULT NULL,
  `deleted_by` varchar(255) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `ID_UNIQUE` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (2,'Trần Văn A','ta@gmail.com','ta@gmail.com','123456','url',0,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-12 15:13:51
